﻿using System;

public class Developer
{
    public int Id;
    public string DeveloperName;
    public string JoiningDate;
    public string ProjectAssigned;

    public Developer()
    {
        
    }

    public Developer(int id, string developerName, string joiningDate, string projectAssigned)
    {
        this.Id = id;
        this.DeveloperName = developerName;
        this.JoiningDate = joiningDate;
        this.ProjectAssigned = projectAssigned;
    }

    public void SetDetails()
    {
        Console.WriteLine("Enter Id");
        Id = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Enter Name");
        DeveloperName = Console.ReadLine();
        Console.WriteLine("Enter Joining Date");
        JoiningDate = Console.ReadLine();
        Console.WriteLine("Enter Name of the assigned project");
        ProjectAssigned = Console.ReadLine();
    }
    public void GetDetails()
    {
        Console.WriteLine("Developer ID: " + Id);
        Console.WriteLine("Developer Name: " + DeveloperName);
        Console.WriteLine("Joining Date: " + JoiningDate);
        Console.WriteLine("Project Assigned: " + ProjectAssigned);
    }
}

public class OnContract : Developer
{
    public int Duration;
    public double ChargesPerDay;
    public double NetSalary;

    public OnContract()
    {
        
    }
    public OnContract(int id, string name, string joiningDate, string projectAssigned, int duration, double chargesPerDay)
        : base(id, name, joiningDate, projectAssigned)
    {
        this.Duration = duration;
        this.ChargesPerDay = chargesPerDay;
    }


    public void SetDetails()
    {
        base.SetDetails();
        Console.WriteLine("Enter duration");
        Duration = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Enter Charges per day");
        ChargesPerDay = Convert.ToDouble(Console.ReadLine());
        NetSalary = CalculateNetSalary();
    }

    public void GetDetails()
    {
        base.GetDetails();
        Console.WriteLine("Duration: " + Duration + " days");
        Console.WriteLine("Charges per day: " + ChargesPerDay);
    }
    public double CalculateNetSalary()
    {
        double netsal = 0;
        netsal = Duration * ChargesPerDay;
        return netsal;
    }
}

public class OnPayroll : Developer
{
    public string Dept;
    public string Manager;
    public double NetSalary,Basic;
    public int Exp;

    public double DA;
    public double HRA;
    public double LTA;
    public double PF;

    public OnPayroll()
    {
        
    }
    public OnPayroll(int id, string developerName, string joiningDate, string projectAssigned, string dept, string manager, double basic, int exp):base(id, developerName, joiningDate, projectAssigned)
    {
        this.Dept = dept;
        this.Manager = manager;
        this.Basic = basic;
        this.Exp = exp;
    }
    public void SetDetails(string dept, string manager, double basic, int exp)
    {
        base.SetDetails();
        Dept = dept;
        Manager = manager;
        Basic = basic;
        Exp = exp;
        NetSalary = CalculateNetSalary();
     
    }

    public void GetDetails()
    {
        base.GetDetails();
        Console.WriteLine("Department: " + Dept);
        Console.WriteLine("Manager: " + Manager);
        Console.WriteLine("Experience: " + Exp + " years");
        Console.WriteLine("Net Salary: " + NetSalary);
    }

    public double CalculateNetSalary()
    {
        double netsal = 0;
        DA = Basic * .2;
        HRA = Basic * .15;
        LTA = Basic * .17;
        PF = 1500;
        netsal=Basic+DA+HRA+LTA-PF;
        return netsal;
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        List<OnContract> cdeves = new List<OnContract>();
        List<OnPayroll> pdeves = new List<OnPayroll>();
        Console.WriteLine("Enter yes to Access Menu");
        string s= Console.ReadLine();
        while(s.Equals("yes"))
        {
            Console.WriteLine("Enter 1 to Create developer \n" +
                "Enter 2 to calculate salary of the Developer \n" +
                "Enter 3 to get developers detail");
            int choice = Convert.ToInt32(Console.ReadLine());
            if(choice == 1)
            {
                Console.WriteLine("Enter 1 to create onContract developer \n" +
                    "Enter 2 to create payroll deveoper");
                int ch=Convert.ToInt32(Console.ReadLine());
                if(ch == 1)
                {
                    OnContract dev = new OnContract();
                    dev.SetDetails();
                    cdeves.Add(dev);
                }
                if(ch == 2)
                {
                    OnPayroll dev = new OnPayroll();
                    dev.SetDetails();
                    pdeves.Add(dev);
                }
                else
                {
                    Console.WriteLine("Enter correct choice");
                }
            }
            else if(choice== 2)
            {
                Console.WriteLine("The salary of the Contractual developers are");
                
               foreach(var dev in cdeves)
                {
                    dev.CalculateNetSalary();
                }
                Console.WriteLine("The salary of the Payroll developers are");
                foreach (var dev in pdeves)
                {
                    dev.CalculateNetSalary();
                }
            }
            else if(choice== 3)
            {
                Console.WriteLine("The details of the contractual developers are");
                
                foreach(var dev in cdeves)
                {
                    dev.GetDetails();
                }
                Console.WriteLine("The details of the payroll developers are");

                foreach (var dev in pdeves)
                {
                    dev.GetDetails();
                }

            }
            else
            {
                Console.WriteLine("Enter yes to continue or enter anything else to quit");
            }
        }

        //Using LINQ 1. Dispaly all records 
        foreach (var dev in cdeves)
        {
            dev.GetDetails();
        }
        foreach (var dev in pdeves)
        {
            dev.GetDetails();
        }

        //2. Display all records where net salary is more than 20000
        var CDevs = cdeves.Where(dev => dev.NetSalary > 20000);

        foreach (var dev in CDevs)
        {
            Console.WriteLine(dev.Id + " " + dev.DeveloperName + " " + dev.NetSalary);
        }
        var PDevs = pdeves.Where(dev => dev.NetSalary > 20000);

        foreach (var dev in PDevs)
        {
            Console.WriteLine(dev.Id + " " + dev.DeveloperName + " " + dev.NetSalary);
        }

        //3. 3. Display all records where name contains 'D'
        var CnameContainsD = cdeves.Where(dev => dev.DeveloperName.Contains("D"));

        foreach (var dev in CnameContainsD)
        {
            Console.WriteLine(dev.Id + " " + dev.DeveloperName + " " + dev.NetSalary);
        }

        var PnameContainsD = pdeves.Where(dev => dev.DeveloperName.Contains("D"));

        foreach (var dev in PnameContainsD)
        {
            Console.WriteLine(dev.Id + " " + dev.DeveloperName + " " + dev.NetSalary);
        }
    }
}
